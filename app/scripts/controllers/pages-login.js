'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:PagesLoginCtrl
 * @description
 * # PagesLoginCtrl
 * Controller of the minovateApp
 */
app
    .controller('LoginCtrl', function ($scope, $state, $http, $rootScope, data) {

        console.log(data);

        // managing the data from the server - get style

        $scope.mainStyle = {};
        $scope.mainStyle['font-size'] = data.FontSize;
        $scope.mainStyle['background-color'] = data.BackColor;
        $scope.mainStyle['color'] = data.FontColor;
        $scope.mainStyle['font-family'] = data.FontFamily;

        $scope.title = data.Title;

        $scope.headerStyle = {};
        $scope.headerStyle['font-size'] = data.TitleFontSize;

        $scope.borderStyle = {};
        $scope.borderStyle['border-width'] = data.BorderWidth;
        $scope.borderStyle['border-style'] = data.BorderStyle;
        $scope.borderStyle['border-color'] = data.BorderColor;

        // Data

        $scope.form = {};
        $scope.loginError = '';
        $scope.user = {"email": "rafi20@gmail.com", "password": "123"};

        // Methods

        $scope.login = login;

        // Functions

        function login () {

            var decodedPassword = btoa($scope.user.email + ':' + $scope.user.password);

            $http.get('scripts/jsons/LoginPageOnSubmit.json', {params: {Url: 'Login1', Base64UserPw: decodedPassword}}).then(function (resp) {

                var responseData = resp.data['d'][0];

                if (responseData.userId != "-1") {

                    localStorage.setItem("userid", responseData.userId);
                    localStorage.setItem("secondsrefresh", responseData.SecondsRefresh);
                    localStorage.setItem("logo", data.LeftLogoUrl);
                    $rootScope.logo = localStorage.getItem('logo');
                    $state.go('app.dashboard', {ItemId: 0, MenuIndex: 0});

                }
                else
                    $scope.loginError = responseData.msg;
            });

        }
    });